#######################################################################
README.rst
#######################################################################

***********************************************************************
MicroPython on ESP8266
***********************************************************************

A short presentation at the September 2016 Central Ohio Python
User's Group meeting.

At the time of this presentation, MicroPython on ESP8266 is still in
active development and various Internet sources are out of date (ie,
many examples no longer work).  The examples here were tested.

Original source:

	https://bitbucket.org/nludban/micropython-esp8266


***********************************************************************
Part 1 - ESP8266
***********************************************************************

=======================================================================
Quick History Review
=======================================================================

MicroPython first ran on the PyBoard (presented by Eric Floehr at the
April(?) COhPy meeting), both of which were the result of a very
successful `Kickstarter campaign
<https://www.kickstarter.com/projects/214379695/micro-python-python-for-microcontrollers/description>`_.


The ESP8266 is a low-cost 32-bit processor with integrated WiFi (SoftAP
and/or station modes) and some GPIO, aimed at the IoT market.
Originally documentation was only available in Chinese, but the price of
just a few dollars attracted developers, which created a market for
boards...


The MicroPython developers ran a software-only `Kickstarter campaign
<https://www.kickstarter.com/projects/214379695/micropython-on-the-esp8266-beautifully-easy-iot/description>`_
to fully support the ESP8266, primarily board support but also getting
the standard library socket module working for Internet connectivity.

Many stretch goals achieved:

- Several additional Python modules
- Several environmental sensor drivers (eg: temperature, humidity,
  pressure, light, accelerometer, compass)
- Native code emitter (vs python byte code)
- upip = pip for micropython
- asyncio integration
- A number of video tutorials


Work is still in progress, especially documentation:

- pyb module features moved to machine?
- Where are the video tutorials?
- asyncio?
- webrepl?


-----------------------------------------------------------------------
In The Near Future...
-----------------------------------------------------------------------

The next processor generation, ESP32, has just been released.  More
memory, bluetooth, peripherals, GPIO, dual cores, double the clock
speed...

https://www.adafruit.com/products/3269

Not yet recommended for end users.


=======================================================================
ESP8266 Hardware
=======================================================================

A typical board is a stack of hardware:

- `Tensilica <https://en.wikipedia.org/wiki/Tensilica>`_ Xtensa LX106
  processor core
- Espressif Systems created a chip with WiFi and peripherals
- AI-Thinker created a tiny board with flash, antenna; FCC certified
- Adafruit created the `HUZZAH ESP8266 Breakout
  <https://www.adafruit.com/product/2471>`_ board


.. figure:: adafruit-2282-03.jpg
   :align: center

   ESP8266 and flash chips with PCB trace antenna and miminal support
   components.


Other models and vendors may provide other options:

- USB communications / power / charging
- Flash size
- LiPo battery power
- 3v3 (native) or 5 volt compatibility
- Extra LEDs and reset / user switches
- Peripheral connectors
- Light weight (flight)


.. figure:: adafruit-2471-14.jpg
   :align: center

   The Adafruit HUZZAH ESP8266 Breakout board, top view.


.. figure:: adafruit-2471-05.jpg
   :align: center

   The Adafruit HUZZAH ESP8266 Breakout board, bottom view.


The Adafruit boards are:

- Officially supported by MicroPython (eg, peripheral to pin mappings)
- Breadboard friendly
- Available at MicroCenter


=======================================================================
ESP8266 Software
=======================================================================

- Boot ROM (64 KiB)

  - "BIOS" with networking functionality

- External flash (SPI)

  - 512 KiB or 4 MiB or more (varies by vendor)

- Harvard architecture = separate code and data

  - 96 KiB data RAM (shared with system)
  - 32 KiB code RAM
  - 32 KiB cache (code from flash)


User application code can be developed with the SDK using C and
callback style programming.

But there are easier ways to program the boards:

- Arduino IDE
- basic
- forth
- lisp
- NodeMCU LUA
- https://micropython.org/


***********************************************************************
Part 2 - MicroPython
***********************************************************************

The quickest way to get started is to download a pre-built binary
image.  The official esp8266 bins are at:

	https://micropython.org/download/

Examples were tested with esp8266-2016``0829-v1.8.3-78-g76dcadd.bin.



=======================================================================
Programming the Flash
=======================================================================

-----------------------------------------------------------------------
Install esptool
-----------------------------------------------------------------------

The SPI flash attached to the ESP8266 can be programmed over the serial
port using "esptool".  The Adafruit board is compatible with Arduino
style 6-pin connector (FTDI) USB to serial converters.

.. code:: console

   $ sudo pip2 install esptool


Usage is printed by running "esptool.py -h".  In addition to
programming flash, esptool can generate binary images, read and write
RAM, and print additional information about the board.


-----------------------------------------------------------------------
Identify, Erase, and Program
-----------------------------------------------------------------------

Hold the GPIO0 switch while resetting the ESP8266 (both buttons on the
Adafruit board).  The LED by the GPIO0 switch should turn dim red to
indicate the bootloader is running.


.. code:: console

   $ sudo esptool.py --port=/dev/ttyUSB0 flash_id
   esptool.py v1.1
   Connecting...
   Manufacturer: e0
   Device: 4016

Maybe WinBond, or LG, but most likely device 40xx = 4 megabytes...


.. code:: console

   $ sudo esptool.py --port /dev/ttyUSB0 erase_flash
   esptool.py v1.1
   Connecting...
   Erasing flash (this may take a while)...


.. code:: console

   $ sudo esptool.py --port=/dev/ttyUSB0 --baud=460800 write_flash \
       --flash_size=8m 0 \
       ~/Downloads/esp8266-20160829-v1.8.3-78-g76dcadd.bin
   Connecting...
   Running Cesanta flasher stub...
   Flash params set to 0x0020
   Writing 532480 @ 0x0... 327680 (61 %)
   Wrote 532480 bytes at 0x0 in 12.2 seconds (350.1 kbit/s)...
   Leaving...

Note that flash_size is in bits, so 1 MiB flash, and the bin is just
over 512 KiB...


=======================================================================
Using MicroPython
=======================================================================


-----------------------------------------------------------------------
Documentation Links
-----------------------------------------------------------------------

Top level documentation index for the MicroPython on ESP8266:

	http://docs.micropython.org/en/latest/esp8266/

The full tutorial with installation instructions:

	http://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/index.html

Brief API overview:

	http://docs.micropython.org/en/latest/esp8266/esp8266/quickref.html


-----------------------------------------------------------------------
Serial Port REPL
-----------------------------------------------------------------------

Install and configure a terminal emulator, eg minicom.

.. code:: console

   $ sudo apt-get install minicom
   $ sudo minicom -s
       | A -    Serial Device      : /dev/ttyUSB0
       | E -    Bps/Par/Bits       : 115200 8N1
       | F - Hardware Flow Control : No
       | G - Software Flow Control : No

   $ minicom


-----------------------------------------------------------------------
Builtin Help
-----------------------------------------------------------------------

.. code:: python

   >>> help()
   Welcome to MicroPython!

   For online docs please visit http://docs.micropython.org/en/latest/esp8266/ .
   For diagnostic information to include in bug reports execute 'import port_diag'.

   Basic WiFi configuration:

   import network
   sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
   sta_if.scan()                             # Scan for available access points
   sta_if.connect("<AP_name>", "<password>") # Connect to an AP
   sta_if.isconnected()                      # Check for successful connection
   # Change name/password of ESP8266's AP:
   ap_if = network.WLAN(network.AP_IF)
   ap_if.config(essid="<AP_NAME>", authmode=network.AUTH_WPA_WPA2_PSK, password="<password>")

   Control commands:
     CTRL-A        -- on a blank line, enter raw REPL mode
     CTRL-B        -- on a blank line, enter normal REPL mode
     CTRL-C        -- interrupt a running program
     CTRL-D        -- on a blank line, do a soft reset of the board
     CTRL-E        -- on a blank line, enter paste mode

   For further help on a specific object, type help(obj)
   >>> 


-----------------------------------------------------------------------
Obligatory Blinky Light Demo
-----------------------------------------------------------------------

.. code:: python

   import time
   import machine

   pin = machine.Pin(0, machine.Pin.OUT)
   for i in range(100):
       pin.value(0) # LED on
       time.sleep(0.42)
       pin.value(1) # LED off
       time.sleep(0.42)

Note that ^E turns on/off auto indentation so multi-line Python code
can be pasted at the interpreter prompt.  Press ^D when complete.


-----------------------------------------------------------------------
What's In The Filesystem?
-----------------------------------------------------------------------

.. code:: python

   >>> import os
   >>> os.listdir('/')
   ['boot.py']

   >>> print(open('/boot.py').read())
   # This file is executed on every boot (including wake-boot from deepsleep)
   #import esp
   #esp.osdebug(None)
   import gc
   #import webrepl
   #webrepl.start()
   gc.collect()


Several posts say there's no filesystem support other than rebuilding
the source to change files, but it appears to be working:

.. code:: python

   >>> f = open('/test.txt', 'w')
   >>> f.write('hello world!\n')
   13
   >>> f.close()

   >>> import os
   >>> os.listdir('/')
   ['boot.py', 'test.txt']

   >>> import machine
   >>> machine.reset()
   ....
   could not open file 'main.py' for reading

   MicroPython v1.8.3-78-g76dcadd on 2016-08-29; ESP module with ESP8266
   Type "help()" for more information.

   >>> import os
   >>> os.listdir('/')
   ['boot.py', 'test.txt']
   >>> print(open('/test.txt').read())
   hello world!


-----------------------------------------------------------------------
What Modules Are Available?
-----------------------------------------------------------------------

The official, complete list:

    http://docs.micropython.org/en/latest/esp8266/library/index.html


Standard library modules:

- os
- sys
- time
- math
- socket
- json

Note many are micro versions (eg, usocket) which can be imported using
the standard name.



MicroPython embedded support modules:

- machine
- network

Note these are generic interfaces for configuring GPIO, resetting the
processor, power management...


Board specific modules:

- pyb (the original pyboard)
- esp (ESP8266 specialized interfaces)


-----------------------------------------------------------------------
Source Code As Documentation
-----------------------------------------------------------------------

https://github.com/micropython/micropython

.. code:: console

   $ git clone https://github.com/micropython/micropython

   $ find micropython -name \*.py
   $ find micropython -name mod\*.c

Note the main interpreter sources appear to be in the "py" subdirectory
and the board specific sources in "esp8266".

There's also a top-level "examples" directory, although some of the
more interesting bits are pyboard specific (ARM/STM32 processor).


.. code:: python

   >>> import esp
   >>> hex(esp.flash_id())
   '0x1640e0'
   >>> esp.flash_size()
   4194304
   >>> esp.check_fw()
   size: 530272
   md5: ed0ec882ee16b350c80c63a3ac43d509
   True


***********************************************************************
Part 3 - Network Examples
***********************************************************************

-----------------------------------------------------------------------
Configure The Network
-----------------------------------------------------------------------

SoftAP mode with security not working... possibly related::

	https://github.com/esp8266/Arduino/issues/1615

	"try esptool with erase_flash flag"

	https://github.com/esp8266/Arduino/issues/1010



Does work as an open network:

.. code:: python

   # Configure commands:
   import network

   ap_if = network.WLAN(network.AP_IF)

   ap_if.config(essid='esp8266-demo',
                authmode=network.AUTH_OPEN)


   # Output, and a client connecting:
   bcn 0
   del if1
   usl
   add if1
   #7 ets_task(4020e374, 29, 3fff7078, 10)
   dhcp server start:(ip:192.168.4.1,mask:255.255.255.0,gw:192.168.4.1)
   bcn 100
   >>> 
   >>> add 1
   aid 1
   station: 78:ca:39:xx:xx:xx join, AID = 1

Try pinging ip:192.168.4.1 from the connected client...

Note the network configuration is persistent and should be
automatically restored after reboot.


TCP server:

.. code:: python

   import socket
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.bind(('0.0.0.0', 80))
   s.listen(2)
   c, addr = s.accept()
   c.recv(100)


TCP client:

.. code:: python

   import socket
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.bind(('192.168.4.1', 80))
   s.send('hello world!')


-----------------------------------------------------------------------
Web REPL
-----------------------------------------------------------------------

Several posts said not yet, but it is present:

>>> import webrepl
>>> webrepl.start()
WebREPL daemon started on ws://192.168.4.1:8266
Started webrepl in setup mode


***********************************************************************
Part 4 - Build From Source
***********************************************************************

This section not finished yet...


=======================================================================
Toolchain
=======================================================================

Official instructions::

	http://www.esp8266.com/wiki/doku.php?id=toolchain

.. code:: console

   $ git clone --recursive https://github.com/pfalcon/esp-open-sdk

~37 MB source code

~3125 MB build tree

~90 MB install

(VENDOR_SDK = 1.5.4)

Additional Raspberry Pi packages required for toolchain build:
gperf, bison, flex, texinfo, help2man, gawk, libtool-bin, ncurses-dev


.. code:: console

   $ make TOP=${HOME}/cross-esp8266 STANDALONE=y | & tee build.cap

   // About 90 minutes later:

   Xtensa toolchain is built, to use it:

   export PATH=/home/pi/cross-esp8266/xtensa-lx106-elf/bin:$PATH

   Espressif ESP8266 SDK is installed, its libraries and headers are merged with the toolchain


=======================================================================
Build MicroPython
=======================================================================

.. code:: console

   $ git clone https://github.com/micropython/micropython




***********************************************************************
References
***********************************************************************

Various resources which may have been used in the writing of this
presentation...

http://www.agcross.com/2015/09/the-esp8266-wifi-chip-part-3-flashing-custom-firmware/

https://github.com/micropython/webrepl -- WebREPL over WebSockets...

http://www.esp8266.com/wiki/doku.php?id=toolchain

http://hackaday.com/2015/03/18/how-to-directly-program-an-inexpensive-esp8266-wifi-module/

http://blog.dushin.net/2016/07/running-micropython-on-the-esp8266/

http://www.danielcasner.org/guidelines-for-writing-code-for-the-esp8266/

https://en.wikipedia.org/wiki/ESP8266

https://en.wikipedia.org/wiki/ESP32

https://github.com/espressif/ESP8266_RTOS_SDK

https://learn.adafruit.com/micropython-basics-how-to-load-micropython-on-a-board/esp8266

https://learn.adafruit.com/micropython-hardware-sd-cards/esp8266
